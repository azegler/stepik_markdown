# Lorem Ipsum

### Paragraphs

**Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.**  

*Hendrerit dolor magna eget est lorem ipsum dolor sit. Senectus et netus et malesuada fames ac turpis egestas maecenas*.  

~~Auctor neque vitae tempus quam pellentesque nec.~~

### Quotation

> *Turpis egestas pretium aenean pharetra magna ac placerat vestibulum lectus. Tellus in metus vulputate eu scelerisque felis imperdiet. Habitant morbi tristique senectus et netus et malesuada fames.*

### Lists

1. Punkt pierwszy.
   1. Podpunkt A.
   2. Podpunkt B.
2. Punkt drugi.

- Element A
  - Podelement 1
  - Podelement 2
- Element B

### Blok Kodu

```py
def sortowanie_babelkowe(lista):
    for i in range(len(lista)):
        for j in range(i):
            if lista[j] > lista[j + 1]:
                x = lista[j]
                lista[j] = lista[j + 1]
                lista[j + 1] = x
    return(lista)
sortowanie_babelkowe()
```
`print(sortowanie_babelkowe)`

### Obraz

![git.jpg](git.jpg)




